package ap.lab2.lib;
/**
 * Provides the service for calculations
 * 
 *
 * */

public interface IcalculatorService {
	/**
	 * @author DWiliiams
	 * @param num1 First number to be added
	 * @param num2 Second number to be added 
	 * @return The sum of two numbers
	 */
	
	
	int add(int num1, int num2);

}
