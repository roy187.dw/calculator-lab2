/**
 * 
 */
package ap.lab2.app.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ap.lab2.app.CalculatorService;

/**
 * @author apstudent
 *
 */
public class CalculatorServiceTest {

	/**
	 * @throws java.lang.Exception
	 */
	private CalculatorService service;
	@Before
	public void setUp() throws Exception {
		service =  new CalculatorService();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAdd() {
		int num1 = 5, num2 = 7;
			int actualResult = service.add(num1, num2);
			int expectedResult = num1 +num2;
			assertEquals(expectedResult, actualResult);
	
	}

}
