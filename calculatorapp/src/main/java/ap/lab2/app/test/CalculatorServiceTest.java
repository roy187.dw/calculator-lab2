package ap.lab2.app.test;



import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import ap.lab2.app.*;
import ap.lab2.lib.CalculatorService;



public class CalculatorServiceTest {
	protected CalculatorService service;
	
	@BeforeClass
	public void setUp()throws Exception  {
		service = new CalculatorService();
	}
	
	@Test
	public void testAdd() {
		int num1 = 5, num2 = 7;
		int actualResult = service.add(num1, num2);
		int expectedResult = num1 +num2;
		assertEquals(expectedResult, actualResult);
	}
}
